package cellular;

import datastructure.IGrid;

import java.util.Random;

import datastructure.CellGrid;

public class BriansBrain implements CellAutomaton {

    /**
	 * The grid of cells
	 */
	IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        // TODO Auto-generated method stub
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for (int row = 0; row < numberOfRows(); row++){
			for (int cols = 0; cols < numberOfColumns(); cols++){
				nextGeneration.set(row, cols, getNextCell(row, cols));
			}
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub
        //Alive becomes dying
        if (getCellState(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        //Dying becomes dead
        else if (getCellState(row, col).equals(CellState.DYING)){
            return CellState.DEAD;
        }
        //Dead with two alive neighbours comes alive
        else if (getCellState(row, col).equals(CellState.DEAD) && countNeighbors(row, col, CellState.ALIVE) == 2){
            return CellState.ALIVE;
        }
        //All other dead remain dead
        else{
            return CellState.DEAD;
        }
    }

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        int rows = currentGeneration.numRows();
		return rows;
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        int cols = currentGeneration.numColumns();
		return cols;
    }

    private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		for (int r = row - 1; r <= row + 1; r++) {
			for (int c = col - 1; c <= row + 1; c++){
				if (getCellState(r, c).equals(state)){
					if(!(r == row && c == col)){
						count += 1;
					}
				}
			}
		}

		return count;
	}

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }
    
}
