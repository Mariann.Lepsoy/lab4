package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int columns;
    int rows;
    CellState initialState;
    CellState[][] Cells;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.columns = columns;
        this.rows = rows;
        this.initialState = initialState;

        Cells = new CellState[rows][columns];

        for (int a = 0; a < rows; a++){
            for (int b = 0; b < columns; b++){
                this.Cells[a][b] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row >= 0 && row < numRows()) {
            if (column >= 0 && column < numColumns()) {
                Cells[row][column] = element;
            }
            else{
                throw new IndexOutOfBoundsException();
            }
        }
        else{
            throw new IndexOutOfBoundsException();
        }
    } 
        

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row >= 0 && row < numRows()){
            if (column >= 0 && column < numColumns()){
                return Cells[row][column];
            }
            else{
                throw new IndexOutOfBoundsException();
            }
        }
        else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub

        IGrid copyGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
        for (int row = 0; row < this.numRows(); row++){
            for (int column = 0; column < this.numColumns(); column++){
                copyGrid.set(row, column, this.get(row, column));
            }
        }

        return copyGrid;
    }
    
}
